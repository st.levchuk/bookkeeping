export const Api = {
  periods: [
    {
      title: 'Період 1',
      create: '1548972000000',
      exchange_course: '26',
      days_expired: '31',
      period_close: true,
      id: '0_asd',
      bookkeeping: {
        income: [
          {
            title: 'Прибуток всього',
            currency: 'USD',
            value: '1400'
          }
        ],
        taxes: [
          {
            title: 'ЄСВ',
            currency: 'UAH',
            value: '920'
          },
          {
            title: 'ЄП',
            currency: 'UAH',
            value: '2400'
          }
        ],
        savings: [
          {
            title: 'saving 01',
            currency: 'USD',
            value: '300'
          },
          {
            title: 'saving 02',
            currency: 'USD',
            value: '300'
          },
          {
            title: 'saving 03',
            currency: 'USD',
            value: '100'
          }
        ],
        home_payments: [
          {
            title: 'комуналка 01',
            currency: 'UAH',
            value: '100'
          },
          {
            title: 'комуналка 02',
            currency: 'UAH',
            value: '300'
          },
          {
            title: 'комуналка 03',
            currency: 'UAH',
            value: '500'
          },
          {
            title: 'комуналка 04',
            currency: 'UAH',
            value: '320'
          }
        ],
        shopping: [
          {
            title: 'shopping 01',
            currency: 'UAH',
            value: '300'
          },
          {
            title: 'shopping 02',
            currency: 'UAH',
            value: '700'
          },
          {
            title: 'shopping 03',
            currency: 'UAH',
            value: '100'
          }
        ]
      }
    },
    {
      title: 'Період 2',
      create: '1548971000000',
      exchange_course: '26',
      days_expired: '31',
      period_close: true,
      id: '0_dfgd',
      bookkeeping: {
        income: [
          {
            title: 'Прибуток всього',
            currency: 'USD',
            value: '400'
          },
          {
            title: 'Прибуток додатковий',
            currency: 'USD',
            value: '1000'
          }
        ],
        taxes: [
          {
            title: 'ЄСВ',
            currency: 'UAH',
            value: '920'
          },
          {
            title: 'ЄП',
            currency: 'UAH',
            value: '2400'
          }
        ],
        savings: [
          {
            title: 'saving 01',
            currency: 'USD',
            value: '300'
          },
          {
            title: 'saving 02',
            currency: 'USD',
            value: '300'
          },
          {
            title: 'saving 03',
            currency: 'USD',
            value: '100'
          }
        ],
        home_payments: [
          {
            title: 'комуналка 01',
            currency: 'UAH',
            value: '100'
          },
          {
            title: 'комуналка 02',
            currency: 'UAH',
            value: '300'
          },
          {
            title: 'комуналка 03',
            currency: 'UAH',
            value: '500'
          },
          {
            title: 'комуналка 04',
            currency: 'UAH',
            value: '320'
          }
        ],
        shopping: [
          {
            title: 'shopping 01',
            currency: 'UAH',
            value: '300'
          },
          {
            title: 'shopping 02',
            currency: 'UAH',
            value: '700'
          },
          {
            title: 'shopping 03',
            currency: 'UAH',
            value: '100'
          }
        ]
      }
    },
    {
      title: 'Період 3',
      create: '1548971000000',
      exchange_course: '26',
      days_expired: '31',
      period_close: false,
      id: '0_dfgdfs',
      bookkeeping: {
        income: [
          {
            title: 'Прибуток всього',
            currency: 'USD',
            value: '1400'
          }
        ],
        taxes: [
          {
            title: 'ЄСВ',
            currency: 'UAH',
            value: '920'
          },
          {
            title: 'ЄП',
            currency: 'UAH',
            value: '2400'
          }
        ],
        savings: [
          {
            title: 'saving 01',
            currency: 'USD',
            value: '300'
          },
          {
            title: 'saving 02',
            currency: 'USD',
            value: '300'
          },
          {
            title: 'saving 03',
            currency: 'USD',
            value: '100'
          }
        ],
        home_payments: [
          {
            title: 'комуналка 01',
            currency: 'UAH',
            value: '100'
          },
          {
            title: 'комуналка 02',
            currency: 'UAH',
            value: '300'
          },
          {
            title: 'комуналка 03',
            currency: 'UAH',
            value: '500'
          },
          {
            title: 'комуналка 04',
            currency: 'UAH',
            value: '320'
          }
        ],
        shopping: [
          {
            title: 'shopping 01',
            currency: 'UAH',
            value: '300'
          },
          {
            title: 'shopping 02',
            currency: 'UAH',
            value: '700'
          },
          {
            title: 'shopping 03',
            currency: 'UAH',
            value: '100'
          }
        ]
      }
    },
    {
      title: 'Період 4',
      create: '1551564000000',
      exchange_course: '26',
      days_expired: '31',
      period_close: false,
      id: '0_bngd',
      bookkeeping: {
        income: [
          {
            title: 'Прибуток всього',
            currency: 'USD',
            value: '1400'
          }
        ],
        taxes: [
          {
            title: 'ЄСВ',
            currency: 'UAH',
            value: '920'
          },
          {
            title: 'ЄП',
            currency: 'UAH',
            value: '2400'
          }
        ],
        savings: [
          {
            title: 'saving 01',
            currency: 'USD',
            value: '300'
          },
          {
            title: 'saving 02',
            currency: 'USD',
            value: '300'
          }
        ],
        home_payments: [
          {
            title: 'комуналка 01',
            currency: 'UAH',
            value: '100'
          }
        ],
        shopping: [
          {
            title: 'shopping 01',
            currency: 'UAH',
            value: '300'
          },
          {
            title: 'shopping 02',
            currency: 'UAH',
            value: '700'
          },
          {
            title: 'shopping 03',
            currency: 'UAH',
            value: '100'
          }
        ]
      }
    }
  ],
  apiGetPeriods: function () {
    return this.periods
  }
}
