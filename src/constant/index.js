export default {
  apiEndPoint: {
    exchange_course: 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'
  },
  currency: {
    USD: 'USD',
    UAH: 'UAH'
  },
  taxes: {
    CSP: '920',
    CT: '5'
  },
  structure: [
    {
      title: 'Надходження',
      key: 'income',
      currency: 'USD',
      defaultFieldName: 'Надходження'
    },
    {
      title: 'Податки',
      key: 'taxes',
      currency: 'UAH',
      defaultFieldName: 'Податок'
    },
    {
      title: 'Заощадження',
      key: 'savings',
      currency: 'USD',
      defaultFieldName: 'Заощадження'
    },
    {
      title: 'Комунальні платежі',
      key: 'home_payments',
      currency: 'UAH',
      defaultFieldName: 'Платіж'
    },
    {
      title: 'Крупні покупки',
      key: 'shopping',
      currency: 'UAH',
      defaultFieldName: 'Покупка'
    }
  ]
}
