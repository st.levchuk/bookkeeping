import Vue from 'vue'
import Vuex from 'vuex'
import store from './store'
import router from './router'
import App from './App.vue'

import './styles/styles.scss'

Vue.config.productionTip = false

Vue.use(Vuex)

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
