export const getCurrentDayTime = () => {
  const createDate = new Date()
  createDate.setHours(0, 0, 0, 0)

  return createDate.getTime()
}
