export default {
  getPeriods (state) {
    return state.periods
  },
  getDetailView (state) {
    return state.detailView
  }
}
