import { Api } from '../api/api'

export default {
  loadPeriods (context) {
    const periods = Api.apiGetPeriods()

    if (periods.length) {
      context.commit('setPeriods', periods)
    }
  },
  addPeriod (context, period) {
    context.commit('setNewPeriod', period)
  },
  deletePeriod (context, period) {
    context.commit('removePeriod', period)
  },
  changePeriod (context, period) {
    context.commit('setPeriodChange', period)
  },
  changeDetailView (context) {
    context.commit('setDetailView')
  }
}
