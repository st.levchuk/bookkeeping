export default {
  setPeriods (state, periods) {
    state.periods.push(...periods)
  },
  setNewPeriod (state, period) {
    state.periods.push(period)
  },
  setPeriodChange (state, period) {
    const index = state.periods.findIndex(item => item.id === period.id)

    if (index !== undefined || index !== null) {
      state.periods.splice(index, 1, period)
    }
  },
  setDetailView (state) {
    state.detailView = !state.detailView
  },
  removePeriod (state, period) {
    const index = state.periods.findIndex(item => item.id === period.id)

    if (index !== undefined || index !== null) {
      state.periods.splice(index, 1)
    }
  }
}
