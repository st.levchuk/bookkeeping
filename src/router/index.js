import Vue from 'vue'
import VueRouter from 'vue-router'

// components
import CreatePeriod from '../components/CreatePeriod/CreatePeriod'
import Periods from '../components/Periods/Periods'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      name: 'Home',
      path: '/',
      component: Periods
    },
    {
      name: 'Create',
      path: '/create',
      component: CreatePeriod
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

export default router
